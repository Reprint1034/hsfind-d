import           Test.Hspec
--import           Test.Hspec.QuickCheck
--import qualified Test.QuickCheck       as Q
import           TextExtraction
import           InvertedIndex
import qualified Data.Map.Strict    as M
import qualified Data.IntMap.Strict as IM
import qualified Data.Text          as T
import qualified Data.Set           as S
--import           Data.Char (isLetter)

testDocs :: FilePath
testDocs = "./test/test-docs" -- For success
--testDocs = "./test/test-docs-fail" -- For fail

main :: IO ()
main = hspec $ do
  describe "PDF text manipulation from a test directory" $ do
    it "number of dirToText keys = number of PDF files in the directory" $ do
      dMap <- dirToText testDocs
      mExt <- matchExtension "pdf" testDocs
      M.size dMap `shouldBe` length mExt

    it "allTermFrequency = length of texts in DocumentMap" $ do
      dMap <- dirToText testDocs
      aTF <- pure $ allTermFrequency dMap
      M.elems aTF `shouldSatisfy`
        (== (M.elems $ M.map (length . T.words . T.concat . IM.elems) dMap))

    it "invertedIndex Occurences keys ⊂ DocumentMap keys" $ do
      dMap <- dirToText testDocs
      invIndex <- pure $ invertedIndex dMap
      (S.fromList $ concat $ map (M.keys) $ M.elems invIndex) `shouldSatisfy`
        \oc -> S.isSubsetOf oc $ M.keysSet dMap

    it "lcInvertedIndex keys = InvertedIndex keys in lowercase" $ do
      dMap <- dirToText testDocs
      invIndex <- pure $ invertedIndex dMap
      lcInvIndex <- pure $ lcInvertedIndex invIndex
      M.keysSet lcInvIndex `shouldSatisfy`
        (\ks -> let invIndexKeysLc = S.map T.toLower $ M.keysSet invIndex
                in S.isSubsetOf ks invIndexKeysLc
                   -- Words without caseable letters containing caseless
                   -- letters end up being purged so check if the purged
                   -- words are indeed words without caseable letters but
                   -- contains caseless letters (determined with backslash)
                   && all (not . T.any (=='\\'))
                          (S.difference invIndexKeysLc ks)
        )

    it "suffixInvertedIndex keys ⊃ lcInvertedIndex keys" $ do
      dMap <- dirToText testDocs
      invIndex <- pure $ invertedIndex dMap
      lcInvIndex <- pure $ lcInvertedIndex invIndex
      suffixInvIndex <- pure $ lcInvertedIndex lcInvIndex
      M.keysSet suffixInvIndex `shouldSatisfy`
        S.isSubsetOf (M.keysSet lcInvIndex)
