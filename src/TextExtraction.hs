-- TextExtration.hs |

{-# LANGUAGE OverloadedStrings #-}

module TextExtraction where

import qualified Data.Text                              as T
import qualified Data.Map.Strict                        as M
import qualified Data.IntMap.Strict                     as IM
import           Data.Maybe                 (catMaybes)
import           Control.Monad
import           Control.Exception
import           System.FilePath
import           System.Directory.Recursive
import           Pdf.Document
import           Pdf.Core                   (Ref)
import           Debug.Trace

type DocumentMap = M.Map FilePath PageText
type PageText = IM.IntMap T.Text

matchExtension :: String -> FilePath -> IO [FilePath]
matchExtension ext dir = do
  subdirs <- liftM2 (:) (pure dir) $ getSubdirsRecursive dir
  matchFiles <- mapM (getDirFiltered (pure . isExtensionOf ext)) subdirs
  return $ concat matchFiles

dirToText :: FilePath -> IO DocumentMap
dirToText dir = trace ("start extraction") $ do
  dirList <- matchExtension "pdf" dir
  listTexts <- mapM tryPdfToText dirList
  filteredListTexts <- pure $ catMaybes listTexts
  return $ M.fromList filteredListTexts


-- PDF Extraction
tryPdfToText :: FilePath -> IO (Maybe (FilePath, PageText))
tryPdfToText file = do
  tryToText <-
    try $ pdfToText file :: IO (Either SomeException (FilePath, PageText))
  case tryToText of
    Left  e -> trace ("extraction fail because " ++ show e)
               $ return $ Nothing
    Right t -> trace ("extraction success") $ return $ Just t

pdfToText :: FilePath -> IO (FilePath, PageText)
pdfToText file = trace ("loading " ++ show file) $
  withPdfFile file $ \pdf -> do
  docu <- document pdf
  catalog <- documentCatalog docu
  rootNode <- catalogPageNode catalog
  refs <- pageNodeKids rootNode
  textList <- pdfRefToText pdf refs
  return (file, IM.fromList $ zip [1..] textList)

pdfRefToText :: Pdf -> [Ref] -> IO [T.Text]
pdfRefToText _   []     = return []
pdfRefToText pdf (x:xs) = do
  tree <- loadPageNode pdf x
  next <- pdfRefToText pdf xs
  case tree of
    PageTreeNode n    -> do
      refs <- pageNodeKids n
      nodeRecursion <- pdfRefToText pdf refs
      return $! nodeRecursion ++ next
    PageTreeLeaf page -> do
      txt <- pageExtractText page
      return $! (T.intercalate " " $ T.lines txt) : next
