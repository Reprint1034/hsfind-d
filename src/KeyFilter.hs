-- KeyFilter.hs |

module KeyFilter where

import qualified Data.Text as T
import qualified Data.Map.Strict as M

data FilterType = Include | Exclude
data FilterOptions = Only [String] | All

keyFilterBoth :: FilterOptions
              -> FilterOptions
              -> (M.Map FilePath T.Text)
              -> (M.Map FilePath T.Text)
keyFilterBoth i e = keyFilter e Exclude . keyFilter i Include

keyFilter :: FilterOptions
          -> FilterType
          -> (M.Map FilePath T.Text)
          -> (M.Map FilePath T.Text)
keyFilter All       _       textMap = textMap
keyFilter (Only xs) Include textMap = M.filterWithKey (\k _ -> k `elem` xs) textMap
keyFilter (Only xs) Exclude textMap = M.filterWithKey (\k _ -> not $ k `elem` xs) textMap
