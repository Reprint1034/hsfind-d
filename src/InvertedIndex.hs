-- InvertedIndex.hs |

{-# LANGUAGE OverloadedStrings #-}

module InvertedIndex where

import qualified Data.Text                              as T
import qualified Data.Text.Lazy                         as TL
import qualified Data.Map.Strict                        as M
import qualified Data.IntMap.Strict                     as IM
import qualified Data.Set                               as S
import           Data.Char          (isLower, isLetter)
import           Debug.Trace
import           TextExtraction

type AllTermFrequency = M.Map FilePath Int

allTermFrequency :: DocumentMap -> AllTermFrequency
allTermFrequency = trace ("performing all term frequency")
                   . M.map (length . allTerms)
  where
    allTerms = IM.foldr (\t -> (++) (T.words t)) []

type InvertedIndex = M.Map Term Occurences
type Term = T.Text
type Occurences = M.Map FilePath PageOccurences
type PageOccurences = IM.IntMap Int

suffixInvertedIndex :: InvertedIndex -> InvertedIndex
suffixInvertedIndex = trace ("performing suffix inverted index")
                      . unioniseSuffixesInvertedIndex
                      . expandSuffixesInvertedIndex

unioniseSuffixesInvertedIndex :: M.Map Term InvertedIndex -> InvertedIndex
unioniseSuffixesInvertedIndex = M.unionsWith mergeOccurences

expandSuffixesInvertedIndex :: InvertedIndex -> M.Map Term InvertedIndex
expandSuffixesInvertedIndex = M.mapWithKey genSuffixKeys

genSuffixKeys :: Term -> Occurences -> InvertedIndex
genSuffixKeys term occurs = M.fromSet (\_ -> occurs) $ suffixes term
  where
    suffixes = S.fromList . T.tails

lcInvertedIndex :: InvertedIndex -> InvertedIndex
lcInvertedIndex invIndex = trace ("performing lowercase inverted index")
                           $ M.difference
                           (M.unionWith mergeOccurences invIndex
                                                        $ lowered
                                                        $ nonLcIndex invIndex)
                           $ nonLcIndex invIndex
  where
    lowered = M.mapKeys (T.toLower)
    nonLcIndex = M.filterWithKey (\k _ -> not $ T.all isLower
                                                      $ T.filter isLetter k)

mergeOccurences :: Occurences -> Occurences -> Occurences
mergeOccurences = M.unionWith (IM.unionWith (+))

invertedIndex :: DocumentMap -> InvertedIndex
invertedIndex = trace ("performing inverted index")
                . invertedIndexRaw . wordCountOnMapPlus
  where invertedIndexRaw = M.map M.fromList . M.unionsWith (++) . M.elems

wordCountOnMapPlus ::
  DocumentMap
  -> M.Map FilePath (M.Map T.Text [(FilePath, IM.IntMap Int)])
wordCountOnMapPlus = M.mapWithKey (\k v -> (\i -> [(k, i)]) <$> v)
                     . arrangeMapWordFull

arrangeMapWordFull ::
  DocumentMap
  -> M.Map FilePath (M.Map T.Text (IM.IntMap Int))
arrangeMapWordFull = M.map arrangeMapWord . wordCountOnMap

arrangeMapWord ::
  IM.IntMap (M.Map T.Text Int)
  -> M.Map T.Text (IM.IntMap Int)
arrangeMapWord wcMap = M.fromList $ map (\x -> (x, filterWord x wcMap))
                       $ wordsOnWordCount wcMap

wordsOnWordCount ::
  IM.IntMap (M.Map T.Text Int)
  -> [T.Text]
wordsOnWordCount = concat . map (M.keys) . IM.elems

filterWord ::
  T.Text
  -> IM.IntMap (M.Map T.Text Int)
  -> IM.IntMap Int
filterWord text = IM.mapMaybe (\v -> v M.!? text)

wordCountOnMap ::
  DocumentMap
  -> M.Map FilePath (IM.IntMap (M.Map T.Text Int))
wordCountOnMap = M.map wordCountPages

wordCountPages ::
  IM.IntMap T.Text
  -> IM.IntMap (M.Map T.Text Int)
wordCountPages = IM.map (wordCount . T.words)

wordCount ::
  [T.Text]
  -> M.Map T.Text Int
wordCount = M.mapKeys TL.toStrict
            . M.fromListWith (+)
            . (`zip` repeat 1)
            . map TL.fromStrict
