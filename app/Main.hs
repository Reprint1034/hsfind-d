{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ForeignFunctionInterface #-}

module Main where

import qualified Data.Text as T
import qualified Data.Map.Strict as M
import TextExtraction
import InvertedIndex
import KeyFilter
import Data.Aeson
import Servant
import Network.Wai.Handler.Warp
import Control.Monad.IO.Class
import System.Environment
import Debug.Trace

main :: IO ()
main = do
  args <- getArgs
  case args of
    []        -> putStrLn "No given directory"
    (x:xs)    -> trace (show xs) $ do
      dMap <- dirToText x
      aTF <- pure $! allTermFrequency dMap
      invIndex <- pure $! invertedIndex dMap
      lcInvIndex <- pure $! lcInvertedIndex invIndex
      sfxInvIndex <- pure $! suffixInvertedIndex lcInvIndex
      putStrLn ("running webserver")
      case xs of
        []    -> run 3000 $ app dMap aTF invIndex lcInvIndex sfxInvIndex
        (y:_) -> run (read y :: Int)
                    $ app dMap aTF invIndex lcInvIndex sfxInvIndex

type DaemonAPI = "documentmap"          :> Get '[JSON] DocumentMap
            :<|> "alltermfrequency"     :> Get '[JSON] AllTermFrequency
            :<|> "invertedindex"        :> Get '[JSON] InvertedIndex
            :<|> "lcinvertedindex"      :> Get '[JSON] InvertedIndex
            :<|> "suffixinvertedindex"  :> Get '[JSON] InvertedIndex

server ::
  DocumentMap
  -> AllTermFrequency
  -> InvertedIndex
  -> InvertedIndex
  -> InvertedIndex
  -> Server DaemonAPI
server dMap aTF invIndex lcInvIndex sfxInvIndex = return dMap
                                              :<|> return aTF
                                              :<|> return invIndex
                                              :<|> return lcInvIndex
                                              :<|> return sfxInvIndex

daemonAPI :: Proxy DaemonAPI
daemonAPI = Proxy

app ::
  DocumentMap
  -> AllTermFrequency
  -> InvertedIndex
  -> InvertedIndex
  -> InvertedIndex
  -> Application
app d a i li si = serve daemonAPI $ server d a i li si
