module Main (main) where

import Lib

main :: IO ()
main = someFunc






-- Controller

data App = App

mkYesod "App" [parseRoutes| / HomeR GET |]

instance Yesod App

--getHomeR = return $ object ["msg" .= listPDF]
getHomeR = do
  --filenames <- liftIO $ matchExtension "pdf" "/home/school/Documents"
  --return $ object ["msg" .= filenames]
  --match <- liftIO $ textMatchesFromPDF "Haskell" "/home/school/Desktop/dsp/parsermockup/sample2.pdf"
  --return $ object ["msg" .= match]
  --pdfTexts <- liftIO $ getTextListFromPDF sample
  --return $ object ["msg" .= pdfTexts]
  pdfTexts <- liftIO $ invertedIndex <$> dirToText sampleDir
  return $ object ["msg" .= pdfTexts]
  --return $ object ["msg" .= T.pack "aa"]
  --file <- getTextListFromPDF "/home/school/Desktop/dsp/parsermockup/sample.pdf"
  --matches <- liftIO $ pure $ textMatches (T.pack "programming") file
  --return $ object ["msg" .= matches]

main :: IO ()
main = warp 3000 App

--main :: IO ()
--main = do
--  args <- fmap (map read) getArgs
--  warp (head args) App
